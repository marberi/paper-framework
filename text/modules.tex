Under is a brief description of the different modules.

\subsection{bias}
Galaxy overdensities are assumed to be related to the underlying
matter distribution through the relation

\begin{equation}
\delta_g(z, k) = \text{bias}(z,k) \delta_m(z, k)
\end{equation}

\noindent
where $\delta_g$ and $\delta_m$ are the galaxy and matter overdensities
and the variables $k$ and $z$ the scale and redshift. The bias is 
implemented to be function of both scale and redshift. Each galaxy 
population and observation like galaxy count and galaxy magnitudes are
assumed to have a different bias.

The following bias types are implemented
\begin{itemize}
\item[fnl] Bias including contribution from non-gaussianities.
\item[interp] Linear interpolation between bias specified in given redshifts.
\item[free] One bias parameter for each redshift bin. Mainly implemented
  inside the correlation and derivative module.
\end{itemize}

\subsection{chain}
Estimate the probability distribution using MCMC sampling. Based on using
PyMC with a Metropolis-Hastings algorithm.

\subsection{corr}
Angular correlation functions. Consists of two parts, the first are the Limber
code used for the predictions in (OLD PAPER...). These are in fourier space
and split in $C_{gg}$, $C_{g\kappa}$ and $C_{\kappa \kappa}$, where $g$ is
either over overdensities in galaxy counts or magnitudes and $\kappa$ is
the convergence field. These fourier space calculations are glued together
with the code found in the files "cl\_all" and "cls\_combine". Part of the 
design reflects how the code started from galaxy counts including magnification.
In general these forms of Limber approximations leads to parts which is harder
to combine.

Second is a set of files implementing both the exact calculations and also
the Limber approximation within this framework. These will later replace all
the other model calculations. All calculations are in terms of $Cl$ and then
the results are converted to $\omega(\theta)$.

\subsection{cosmo}

Cosmological functions. Currently only $\Lambda \text{cmd}$ is implemented
with the possibility of $gamma$ as a free function. Often, for each set of
parameters the cosmological functions like distance and growth is calculated
at the start and then cached as B-splines. Modules like the correlation 
function use an interface for using these splines.

\subsection{cov}
Covariance function between the correrlations. The standard implementation
is based on Gaussian error and implemented through inline c++ code. Evaluating
the covariance for a high numbers of correlations is time consuming, therefore
it was written in inline c++ through the Scipy weave module. There is also
a pure Python version which only supports one populations and observable and
is only indender for testing purposes. 

\subsection{deriv}
Derivatives used in the fisher matrix. Normally all derivatives are calculated
numerically through finite difference. Derivatives of photo-z transition
probabilities and bias paramerts are done in blocks and included here.

\subsection{fisher}
Code for estimate and handle the Fisher matrices. Relies on the corr,cov and
deriv modules for calculating the correlations, estimating the covariance and
finding the derivatives. Includes several classes for Fisher matrix manipulations.
The fisher\_basic version is general Fisher matrix functionality for e.g.
adding two Fisher matrices, removing parameters, marginalizating over parameters
or removing pareters which are not measuring. The fisher\_cosmo version adds
functionality for Fisher matrices in cosmology, including fixing of bias parameters
and figures of merit.

\subsection{modes}
The framework consists of several modules which can be used in several ways.
Instead of writing many fronends, there is one binary named "front.py", which
can work in different modes. By passing the "-m" option the frontend select
between functions defined in the modes directory that can be radically
different.

\subsection{observ}
Determines which correlations to use. At the high level, the option "opts.exp\_type"
says which obsevable to use. It defines what exp\_type means, for example $gs$
for galaxy and shear is translated into which populations and fluctuations to use.

\begin{itemize}
\item[exp\_type] String with Keyword for set of observations.
\item[obs\_cov] Expansion of exp\_type into a data structure describing what is
 correlated. The format is defined below.
\item[obs\_exp] Expansion of obs\_cov for a given $l$ or $\theta$ values to determine
 exactly which correlations to use. This is done to remove correlations which enters
 into nonlinear scales. The format is defined below.
\item[all\_fluct] All fluctuation types used. Redundant with obs\_cov.
\item[all\_pop] All populations used. Redundant with obs\_cov.
\end{itemize}

\subsubsection{obs\_cov}
The obs\_cov format consist of a list, where each elements are tuples of the form
(obs1, obs2, to\_corr). Here obs1 and obs2 are fluctuations in the first and second
type and are again tuples of the form (ftype, pop), where

\begin{itemize}
\item[ftype] Fluctuation type. Either matter, counts, mag or shear.
\item[pop] Galaxy population.
\end{itemize}

\noindent
The "to\_corr" part is a string describing a subset of the correlations to use. 
Possible values include

\begin{itemize}
\item[auto] Auto correlations.
\item[all] Where $i <= j$.
\item[abs\_all] Absolute all the correlations.
\item[magic] Magic subset. Used to test effect of lensing.
\end{itemize}

In the example under a magic number of counts for the bright galaxy counts is
correlated with a magic number of bright galaxy counts.

\begin{verbatim}
[[('counts', 'bright'), ('counts', 'bright'), ['magic']]]
\end{verbatim}

TODO: Make the part of what to correlate into a list.

\subsection{pkg}
Implements reading and writing the format described in (REF appendix). Also includes
the code for only writing the meta part.

\subsection{pop}
Handle $n(z)$, binning and photo-z for the different populations.

\subsection{power}
Different implementation for the power spectrum. Currently the Eisentein-Hu is the
only used. Earlier CAMB, Halofit and Bond was also working. Some effort will get
them back in a usable state.

\subsection{pres}
Ways of presenting the results. Includes outputting figures of merits in ascii and
contour plots. The meaning will soon slightly change and the presentations will 
become a more central concept.

\subsection{priors}
Include different forms of priors, like the DETF fisher matrices and how to add
priors on bias parameters.

\subsection{probes}
Layer for implementing different probes. Currently it has LSS/WL in 2D, 3D RSD and
BAO trough ugly interfaces with precalculated Fisher matrices.

\subsection{remote}
Proof of concept interface. Instead of directly calculating object when needing
them, all requests for objects go through a dispatcher. It determines if the
object is already calculated an returns the cached version or starts doing the
calculation. Its future depends on the relation to the Brownthrower project.

\subsection{syst}
Observable systematics in the correlation functions. Include shot-noise, galaxy
shape noise and effect of photo-z through the transition matrices.

\subsection{tasks}
For integrating with the overall science pipeline the framework implement different
tasks. These will later be used by the Brownthrower framework, but is currently
only used internally.

\begin{itemize}
\item[chain] Run an mcmc chain.
\item[fisher] Estimate one fisher matrix.
\item[mcmc] Calculate theoretical correlations and estimate one chain.
\item[meta] Output information on population and binning used to determine which
  correlations to estimate.
\item[thpkg] Create a observation package with theoretical estimates.
\end{itemize}

\subsection{tbls}
Very central component when doing the forecasts. Instead of calculating one and one
Fisher matrix, large tables with predicitions gets calculated. Tables are no longer
stored inside the main repository, but moved into separate repositories related
to the papers. Still some code is located in "tbls/lib" for implementing the tables.
Later it will be merged with the presentations in "pres". The progress depends on
the status of the 3D RSD fisher matrix and the Limber approximation. After removing
those this part can remove lots of legacy code. 
