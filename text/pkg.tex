\subsection{Motivation}
Defining a standard way of exchanging correlation functions has several
advantages. It provides a common interface to codes fitting to
parameters. Small differences in variable naming and order of storing the
correlation would results in different codes saving the same information in
different ways. Each correlation would then require a separate input module and
documentation. Meta data is an important part of exchanging data between
one tool and another. When measuring correlations the redshift bins are 
important to specify, for example when fitting to theory. In cases the binning
is implicitly known or specified in e.g. a private email. Tools are then
manually adjusted to use this binning. By adding the required data directly
to the correlation file format, the codes using the measured correlation
functions can automatically adapt to the specified binning. That both saves
time and is more robust. One standard format is likely also better designed
and documented than the ad hoc defined formats.

Storing data requires an underlying format like a text file or a binary format.
Correlations are structured data depending on angles or lvalues, redshifts,
galaxy populations and the observables e.g. galaxy count or shapes. Using
several populations and observables affects the design. Text files is one
long string with lines separated by the new line character ("\\n"). One common
approach is writing the correlations for each angle of l value on one line.
Storing the data is a matter of systematically ordering the correlation one
each line in terms of redshift, populations and observations. The real problem
comes when reading in the data. Any mistake in the ordering is hard to
detect. Using binary formats often allows data structures which ease storing,
parsing and is more robust.

Loading a text file and binary file can be orders of magnitudes different in
time. For a binary you often only need to read the content, while for text
files additional parsing is required. Covariance matrices does already for a
modest amount of redshift bins demand too much process time to load. The
dimention of the covariance matrix is also larger than 1000, which is the
upper limit on arrays in FITS files. Not only based on the covariance size,
the underlying format for the correlation functions will be HDF5.

Using HDF5 is only required when exchanging data. A code can store in binary
when fitting and then in ascii to plot with standard tools like gnuplot. 
Excisting tools often already have defined their own ascii format. Adding
an additional ascii format for plotting would only cause work for nothing
and resistance against the real goal of defining a format for data exchange.
Plotting correlation using ascii is however not completely straight forward.
The correlation you want to plot need to be related to e.g. the column number.
For a few bins, one population and one type of measurement it is easy. With
more bins and populations this relation get more involved. Calculating these
in your head require work and easily gets wrong. Coding a separate small tool
to calculate the column number, but would require inputing the number of bins,
populations and measurement types. 

For plotting using HDF5 is better than anticipated at first sight. Instead
of relying on general tools like gnuplot, more custom solutions can be
created. For example Python can reads HDF5 with e.g. pytables, plot with
matplotlib and create input parsers with argparse. Creating tools for
exactly plotting correlations is not too difficult and can with a common
format be shared. So ascii can be used for plotting, but later someone can
create better tools using HDF5 directly.

The HDF5 format is a hirarchical binary format defined by the HDF group [1]. It
is a completely portable file format with support for languages like Python,
Fortran 90, c, c++, Matlab, Mathematica, IDL and Java. One file looks like
a file system, with directories corresponding to hdf5 groups and files
corresponding to hdf5 nodes. For example specifying

/foo/bar

could mean group "foo" and node "bar". Common types of nodes are N 
dimentional arrays and tables. The tables resemble SQL tables without
relations. For the correlation functions only N dimentional arrays
will be used. In addition all the nodes support adding meta data. The
data is stored as key,value pairs on each node. For more detailed
threatment, please visit external documentation. Python users are
advised to look at the pytables tutorials.

\subsection{JSON}
Metadata stored together with the correlations also require a format.  Possible
syntax variations for specifying the mean of the redshifts bins are

\bd
\item 0.1 0.2 0.3 0.4
\item 0.1, 0.2, 0.3, 0.4 
\item $[0.1, 0.2, 0.3, 0.4]$
\ed

\noindent
The last one is valid JSON [2] syntax for list. JSON is a lightweight format
for interchanging data. It specifies simple syntax for list, integers, boleans,
floats, key-value mappings, strings, chars and a few others. Valid JSON
examples are

\bd
\item int    9
\item float  3.14
\item string "hei"
\item bool   true
\item array [1.2, 2.1, "hei", 9]
\ed

All metadata values should be valid JSON object. Almost all modern programming
languages support reading and writing JSON [3]. In practice for some usages,
JSON objects can be read or written by custom code to avoid installing a
library. Then the advantage is one standard format.


\subsection{Storage of data}
Data is stored using hirarchical groups in the HDF5 format. Each of the
groups 

\begin{verbatim}
/meta   - Meta data for the correlations
/corr   - Correlation functions
/cov    - Covariance matrix
/photoz - Defining the n(z)
\end{verbatim}

\noindent
are described in separate subsections. Each of these sections can be
produced by different tools.

\subsection{/meta - General meta data}
Meta is an empty array to store the metadata as attributes in key, value
format. The reason for not using a two column tables is to avoid specifying
a maximum string size.


The field "fourier" is required and means

\begin{verbatim}
True  - Cl correlations
False - w(theta) correlations
\end{verbatim}

\noindent
Using Cls require specifying
\begin{verbatim}
l_mean  - List with the mean of l bins used.
l_width - List with the width of the l bins used.
\end{verbatim}

\noindent 
and/or while for w(theta) the following fiels are required
\begin{verbatim}
ang_mean  - List with the mean of l bins used.
ang_width - List with the width of the l bins used.
\end{verbatim}

\noindent
Storing both Cls and w(theta) within the same group is not supported.

\subsubsection{/meta/\$pop}
Meta data which differs for each galaxy population is stored in
a subgroup named after the population. As for the general meta data
one need to create an empty array to save the data as attributes.
Required fields for each population are

\begin{verbatim}
z_mean - List with mean of readshift bins.
z_width - List with the width of the redshift bins.
\end{verbatim}

\noindent 
In addition other values can describe the galaxy selection, like
magnitude limits, colors, galaxy types or photo-z quality. These
attributes are left to define for the user. Specifying specific
names for these are not done since each environment where the
code is integrated will follow a different nomenclature. 

\subsection{/corr - Correlation functions}
Correlations are 3D arrays with indices

\begin{verbatim}
1st - l value or angle
2nd - lens redhift bin number
3nd - source redhift bin number
\end{verbatim}

stored in the blocks corr/corr1 to corr/corrn. The blocks corresponds the
correlations of two pairs of

\begin{verbatim}
ftype$i - required - Fluctuation type. 
pop$i   - optional - Galaxy population.
band$i  - optional - Filter used for detection.
\end{verbatim}

where \$i is 0 for source and 1 for lenses. The defined types of ftypes are

\begin{verbatim}
mat    - Matter
counts - Galaxy counts
mag    - Magnitudes
shear  - Shear
size   - Galaxy size
\end{verbatim}
        
\noindent                  
Either population and/or filter used is specified for all galalxy fluctuations
or completely dropped. The strings should be specified using JSON quotes. One
example is

\begin{verbatim}
ftype0 = "counts"
pop0   = "bright"
ftype1 = "mag"
pop1   = "faint"
\end{verbatim}

\noindent
for a block of correlating counts in the foreground with magnitudes of the
sources.

\subsection{/cov - Covariance}
The covariance is either a 5D or 6D array. When the lvalues or angles are
assumed correlated there is two l/angles and the structure is 6D, while when
they are uncorreated the structure is 5D. For the covariance

\begin{equation}
\text{Cov}(\omega_{ij}(\theta_1), \omega_{mn}(\theta_2))
\end{equation}

and correlated angles, the fields are

\begin{itemize}
\item[$\theta_1$] First correlation angle/l value
\item[$\theta_2$] Second correlation angle/l values
\item[i] First redshift bin in the first correlation
\item[j] Second redshift bin in the first correlation
\item[m] First redshift bin in the second correlation
\item[n] Second redshift bin in the second correlation
\end{itemize}

\noindent
In Fourier space the structure is the same and when the angles or l-values
are uncorrelates, the first index is dropped.

\subsection{/photoz - Estimating n(z)}

When estimating the selection function for each redshift bin, the
following photo-z quantities 

\begin{itemize}
\item[peaks] Pairs of (z\_s, z\_p) for each galaxy
\item[pdfs] Probability distribution for each galaxy
\end{itemize}

can be used. Testing stacking pdf has not lead to convincing results and here
we only consider using the peaks. Future versions might use the pdfs. Storing
the peaks can be done in the following manners

\begin{itemize}
\item[/photoz/catalog/\$pop] HDF5 tables with all galaxies fullfilling the
  selection criteria for the population \$pop. Only two fields, $z_p$ the
  photo-z and $z_s$ are required, while other fields can potentially be included.
\end{itemize}

\subsection{/noise - Estimated noise}
In the theoretical estimated observables and if using a theoretical formulation
for the covariance, one need to add the expected noise. The noise is added to
correlations the following way

\begin{equation}
\tilde{C}_{A_i B_j} = C_{A_i B_j} + \eta_{A_i B_j}
\end{equation}

\noindent
where A and B are different types of observations and i,j are redshift bin
indices. Including information about the noise is required for A==B and
otherwise optional. The notation here is in Fourier space, but adding noise is
done equal for $w(\theta)$. Similar to the storage of correlations, the noise
is stored in the blocks noise/noise1 to noise/nosen for each pair A,B. Relating
each block to the measured fluction is done through the attributes

\begin{itemize}
\item[ftype0] Fluctuation type for first index
\item[pop0] Population for first index
\item[ftype1] Fluctuation type for second index
\item[pop1] Population for second index
\end{itemize}

which are required to set on each array. 

\subsection{/slopes - Number count and magnitude slopes}

For overdensities of number counts, magnitudes and sizes the fluctuations can be
written

\begin{equation}
\delta_{A_i} = \delta_{I_i} + \alpha_{A_i} \delta_{\kappa_i}
\end{equation}

where $A$ is the observable and the two contributions are the intrisic
fluctuation and one resulting from lensing. The factor $\alpha$ is here
defined scale independent, but varying with redshift. Slopes are stored
in tables /slope/\$pop, with one tables for each population. The field
"z" is used for the redshift and the fields

\begin{itemize}
\item[counts] Magnification with number counts.
\item[mag] Magnification with magnitude.
\item[size] Magnification with sizes
\end{itemize}

can be included for different types of magnification. The values are
given as measurement in points and is not averaged over redshift bins.
How many redshift points to use is dependent on the one doing the
measurements.

[1]
http://www.hdfgroup.org/HDF5/

[2]
http://www.json.org/
RFC 4627, http://tools.ietf.org/html/rfc4627.html

[3]
Python 2.6 and above includes a json module in the standard library.
